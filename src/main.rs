use terminal_emulator::{term::{Term, SizeInfo, cell::Flags}, Processor, ansi::{Color, NamedColor}};
use std::{time, collections::HashMap, env, io::Read, process::Command, thread, str, io::{self, Write}, sync::{mpsc::{Sender, Receiver, channel}, Arc, Mutex}};
use pty::fork::*;
use raw_tty::IntoRawMode;
use ws::{Settings, Builder, Message, CloseCode, Error};
use uuid::Uuid;

struct Config {}
impl Config {
    /* websocket settings */
    const WS_BIND_URL:&'static str = "0.0.0.0:5555";
    const WS_MAX_CLIENTS:usize = 1000;

    /* terminal settings */
    const TERM_WIDTH:u32 = 100;
    const TERM_HEIGHT:u32 = 32;
    const TERM_TYPE:&'static str = "xterm";
    const TERM_COMMAND:&'static str = "/bin/bash";

    /* performance tuning */
    const UPDATE_DELAY_MSEC:u64 = 250;
    const UPDATE_SIZE_THRESHOLD:usize = 2048;
}

struct Intensity {}
impl Intensity {
    const BOLD:u32 = 1;
    const DIM:u32 = 2;
    const NORMAL:u32 = 22;
}

struct TTYManager<'a,'b> {
    cmd: &'a str,
    termtype: &'b str,
    term: Arc<Mutex<Term> >,
    processor: Processor,
    buf: Vec<u8>,
}

impl<'a,'b> TTYManager<'a,'b> {
    /* Constructor */ 
    fn new(cmd: &'a str, width: u32, height: u32, termtype: &'b str) -> TTYManager<'a,'b> {
        TTYManager { 
            cmd: cmd,
            termtype: termtype,
            term: Arc::new(Mutex::new(Term::new(SizeInfo {width: width as f32 , height: height as f32, cell_height: 1.0, cell_width: 1.0, padding_x: 0.0, padding_y:0.0, dpr: 1.0 }))),
            processor: Processor::new(),
            buf: vec![0; 1024],
        }
    }

    /* Associated functions */
    fn handle_color(current_intensity: u32, current_named: &mut NamedColor, resetted_attrs: &mut bool, new_color: &Color, attributes: &mut Vec<u8>, is_bg: bool) -> u32 {
        /* Only support named color for now, others are mapped to default */
        let new_named = match *new_color {
            Color::Named(color) => color,
            Color::Spec(_) => if is_bg { NamedColor::Background} else { NamedColor::Foreground},
            Color::Indexed(_) => if is_bg { NamedColor::Background} else { NamedColor::Foreground},
        };

        /* If color set to default, only way to reset it it to reset all attributes */
        let default = if is_bg { NamedColor::Background} else { NamedColor::Foreground};
        if !*resetted_attrs && new_named == default && *current_named != default {
            attributes.extend(b"0;");
            *resetted_attrs = true;
        }

        let mut intensity = current_intensity;
        let code_offset = if is_bg { 40 } else { 30 };

        /* Send code only if color has changed, or if forced by attribute reset */
        if *resetted_attrs || *current_named != new_named {
            if new_named >= NamedColor::Black && new_named <= NamedColor::White {
                attributes.extend(format!("{};", (new_named as usize) + code_offset).as_bytes());
            }
            if new_named >= NamedColor::BrightBlack && new_named <= NamedColor::BrightWhite {
                attributes.extend(format!("{};", (new_named as usize) + code_offset - 8).as_bytes());
                intensity = Intensity::BOLD;
            }
            if new_named >= NamedColor::DimBlack && new_named <= NamedColor::DimWhite {
                attributes.extend(format!("{};", (new_named as usize) + code_offset - 16).as_bytes());
                intensity = Intensity::DIM;
            }
        }
        *current_named = new_named;
        return intensity;
    }

    fn get_redraw(term_handle: &mut Arc<Mutex<Term> >, output: &mut Vec<u8>) {
        output.extend("\x1B[H\x1B[2J".as_bytes()); /* clear screen */
        let mut buf = [0; 16]; 
        let term = term_handle.lock().unwrap();
        let mut prev_row = 0;
        let mut prev_col = 0;
        let mut prev_intensity = 0;
        let mut attrs_resetted = true;
        let mut prev_bg = NamedColor::Background;
        let mut prev_fg = NamedColor::Foreground;
        let mut prev_flags = Flags::empty();
        for cell in (*term).renderable_cells() {
            if cell.bg == Color::Named(NamedColor::Cursor) {
                continue;
            }
            let row = cell.line.0;
            let col = cell.column.0;


            let mut attributes: Vec<u8> = Vec::new();

            /* We do this because terminal-emulator lib omits whitespace cells */
            if prev_row != row || prev_col != col {
                /* We assume that every skipped character has default attribute */
                output.extend(b"\x1B[0m");
                attrs_resetted = true;
            }

            if prev_row != row {
                for _ in prev_row..row {
                    output.extend(b"\r\n");
                }
                prev_col = 0;
            } 
            if prev_col != col {
                for _ in prev_col..col {
                    output.extend(b" ");
                }
            }
            prev_row = row;


            /* send colors */

            /* Color can affect intensity (i.e. BrightXXX / DimXXX colors) */ 
            let mut intensity = Self::handle_color(Intensity::NORMAL, &mut prev_bg, &mut attrs_resetted, &cell.bg, &mut attributes, true);
            intensity = Self::handle_color(intensity, &mut prev_fg, &mut attrs_resetted, &cell.fg, &mut attributes, false);

            /* Intensity specified in flags take precedence over intensity specified in the color */
            if cell.flags & Flags::BOLD != Flags::empty() {
                intensity = 1;
            } else if cell.flags & Flags::DIM != Flags::empty() {
                intensity = 2;
            }

            if attrs_resetted || intensity != prev_intensity {
                attributes.extend(format!("{};", intensity).as_bytes());
            }
            prev_intensity = intensity;

            /* send attributes */
            if attrs_resetted || cell.flags & Flags::INVERSE != prev_flags & Flags::INVERSE {
                attributes.extend((if cell.flags & Flags::INVERSE != Flags::empty() { "7;" } else { "27;" }).as_bytes());
            }

            if attrs_resetted || cell.flags & Flags::UNDERLINE != prev_flags & Flags::UNDERLINE {
                attributes.extend((if cell.flags & Flags::UNDERLINE != Flags::empty() { "4;" } else { "24;" }).as_bytes());
            }
            prev_flags = cell.flags;

            if attributes.len() > 0 {
                output.extend(b"\x1B[");
                output.extend(&attributes[0..(attributes.len() - 1)]);
                output.extend(b"m");
            }

            /* send character */
            let mut first_char = false;
            for ch in cell.chars.iter() {
                if *ch != ' ' || !first_char {
                    first_char = true;
                    prev_col = col + 1;
                    let r = ch.encode_utf8(&mut buf);
                    for i in 0..(r.len()) {
                        output.push(buf[i]);
                    }
                }
            }
            attrs_resetted = false;
        }

        /* Send cursor position */
        let cursor = term.cursor().point;
        output.extend(format!("\x1B[{};{}H", cursor.line + 1, cursor.col + 1).as_bytes());
    }

    /* Methods */ 
    fn get_term_handle(&self) -> Arc<Mutex<Term> > { 
        return self.term.clone();
    }

    fn on_tty_activity(&mut self, data: &[u8]) {
        let mut term = self.term.lock().unwrap();
        for byte in data {
            self.processor.advance(&mut *term, *byte, &mut self.buf);
        }
    }

    fn run(&mut self, sender: Sender<Vec<u8> >) -> io::Result<()> {
        let fork = Fork::from_ptmx().expect("Unable to allocate tty");
        if let Some(mut master) = fork.is_parent().ok() {
            let mut stdin = io::stdin().into_raw_mode()?;
            let mut writer = master.clone();
            let mut buf = vec![0; 1024];

            let _ = thread::spawn(move || {
                let mut buf = vec![0; 1024];
                loop {
                    match stdin.read(&mut buf) {
                        Ok(0) => panic!("stdin went away"),
                        Ok(r) => {
                            writer.write(&buf[0..r]).expect("Couldn't write to pty"); 
                            ()
                        },
                        Err(e) => panic!("read error (stdin): {}", e),
                    }
                }
            });

            loop {
                match master.read(&mut buf) {
                    Ok(0) => {
                        println!("Process exited.");
                        std::process::exit(0);
                    },
                    Ok(_nread) => {
                        io::stdout().write_all(&buf[0.._nread])?;
                        io::stdout().flush()?;
                        self.on_tty_activity(&buf[0.._nread]);
                        sender.send(buf[0.._nread].to_vec()).unwrap();
                    },
                    Err(e)     => panic!("read error (pty): {}", e),
                }
            }

        } else {
            env::set_var("TERM", self.termtype);
            env::set_var("COLUMNS", format!("{}", Config::TERM_WIDTH));
            env::set_var("LINES", format!("{}", Config::TERM_HEIGHT));
            Command::new(self.cmd).status()?;
        }
        Ok(())
    }
}

struct TTYWSServer { 
    clients: Arc<Mutex<HashMap<uuid::Uuid, ws::Sender>>>,
    me: uuid::Uuid,
} 

impl ws::Handler for TTYWSServer { 
    fn on_close(&mut self, _code: CloseCode, _reason: &str) {
        let mut clients = self.clients.lock().unwrap();
        clients.remove(&self.me);
    }
    fn on_error(&mut self, _err: Error) {
        let mut clients = self.clients.lock().unwrap();
        clients.remove(&self.me);
    }
} 

fn spawn_acceptor_thread(mut term_handle: Arc<Mutex<Term> >) -> Arc<Mutex<HashMap<uuid::Uuid, ws::Sender> > > {
    let clients = Arc::new(Mutex::new(HashMap::<uuid::Uuid, ws::Sender>::new()));
    let returned_clients = clients.clone();
    let _ = thread::spawn(move || {
        Builder::new().with_settings(Settings{ max_connections: Config::WS_MAX_CLIENTS, ..Settings::default()})
        .build(|out:ws::Sender| {
            let client_id = Uuid::new_v4();
            let serv = TTYWSServer { clients: clients.clone(), me: client_id};
            let mut clients = clients.lock().unwrap();

            /* Need to send redraw because we have a new client */
            let mut redraw = Vec::new();
            TTYManager::get_redraw(&mut term_handle, &mut redraw);
            out.send(Message::binary(&redraw[..])).unwrap();

            clients.insert(client_id, out);
            serv
        }).unwrap().listen(Config::WS_BIND_URL).unwrap();
    });
    returned_clients
}

fn spawn_tty_thread() -> (Receiver<Vec<u8> >, Arc<Mutex<Term> >) {
    let mut ttymgr = TTYManager::new(Config::TERM_COMMAND, Config::TERM_WIDTH, Config::TERM_HEIGHT, Config::TERM_TYPE);
    let term_handle = ttymgr.get_term_handle();
    let (sender, receiver) = channel();
    let _ = thread::spawn(move || {
        ttymgr.run(sender).unwrap();
    });
    (receiver, term_handle)
}

fn main_loop(clients: Arc<Mutex<HashMap<uuid::Uuid, ws::Sender> > >, mut term_handle: Arc<Mutex<Term> >, receiver: Receiver<Vec<u8> >) {
    loop {
        thread::sleep(time::Duration::from_millis(Config::UPDATE_DELAY_MSEC));
        let mut data = receiver.recv().unwrap();
        /* attempt to empty input queue */
        loop {
            match receiver.try_recv() {
                Ok(d) =>  {
                    data.extend(d);
                },
                _ => {
                    break;
                }
            }
        }

        /* If update is big, try to send full redraw instead */ 
        if data.len() > Config::UPDATE_SIZE_THRESHOLD {
            let mut redraw = Vec::new();
            TTYManager::get_redraw(&mut term_handle, &mut redraw);
            if redraw.len() < data.len() {
                data = redraw;
            }
        }

        let clients = clients.lock().unwrap();
        for (_,client) in &*clients {
            let msg = Message::binary(&data[..]);
            client.send(msg).unwrap();
        }
    }
}

fn main() {
    let (updates_receiver, term_handle) = spawn_tty_thread();

    let clients = spawn_acceptor_thread(term_handle.clone());

    main_loop(clients.clone(), term_handle.clone(), updates_receiver); 
}

