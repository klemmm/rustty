Usage (server):

1) Edit Config struct in src/main.rs (default websocket port is 5555)

2) Launch: ./rustty 

Usage (client):

1) Host webclient.html somewhere

2) Go to http://blahblah/webclient.html?host=127.0.0.1&port=5555 (replace host and port if needed)
